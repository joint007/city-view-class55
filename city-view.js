const accessKey = '8REe62qCVZNn9-9xjsmWAM7R0vp1hJAswbv3_zobJ1E'
const baseURL = 'https://api.unsplash.com/'
const eleInput = document.querySelector('.inputCity')
const eleSearch = document.querySelector('.fa-search')
const eleImgLibrary = document.querySelector('.imgLibrary')
const eleBody = document.querySelector('body')
const eleDes = document.querySelector('.caption')
const eleBtnForward = document.querySelector('.fa-forward')
const user_input = 'user_input'
let page = 1
let input_result = ''
let input_all_results = []


const searchFun = () => {
    eleInput.select() //select all texts
    input_result = eleInput.value.trim().toLowerCase()
    // console.log('key word====>',input_result)
    input_all_results.push(input_result)

    localStorage.setItem(user_input, JSON.stringify(input_result))
    localStorage.setItem('user_input_all_history', JSON.stringify(input_all_results))
    page = 1
    fetchCity()
}

const inputSearch = (evt) => {
    // console.log(evt.key)
    evt.key === 'Enter' && searchFun()
}

const switchBg = (selectedItem)=> {
    eleDes.classList.remove('blur')
    setTimeout(()=>{
        eleBody.style.backgroundImage = `url('${selectedItem.urls.regular}')`
        eleDes.innerHTML = selectedItem.alt_description
        eleDes.classList.add('blur')
    },20)
}

const nextPage = () => {
    page++
    fetchCity()
}

const fetchCity = () => {
    eleDes.classList.remove('blur')
    eleImgLibrary.innerHTML = ''
    input_result = JSON.parse(localStorage.getItem(user_input))
    !input_result && (input_result = 'Toronto')
    fetch(`${baseURL}search/photos?client_id=${accessKey}&query=${input_result}&orientation=landscape&page=${page}`)
        .then(response => response.json())
        .then(data => {
                let result = data.results[0].urls.regular
                let des = data.results[0].alt_description
                eleBody.style.backgroundImage = `url('${result}')`
                eleDes.innerHTML = des
                eleDes.classList.add('blur')
                data.results.forEach((item, index) => {
                    let eleImgItem = document.createElement('img')
                    eleImgItem.src = item.urls.thumb
                    eleImgItem.className = 'card'
                    eleImgItem.addEventListener('click', ()=>{
                        switchBg(item)
                    })
                    eleImgLibrary.appendChild(eleImgItem)
                })
            }
        )
}

eleSearch.addEventListener('click', searchFun)
eleInput.addEventListener('keydown', inputSearch)
eleInput.addEventListener('focusin', () => {
    eleInput.select()
})
eleBtnForward.addEventListener('click', nextPage)
fetchCity()




